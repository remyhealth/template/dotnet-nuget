using System.Collections.Generic;

namespace DotnetNuget.Common
{
    public class DotnetNugetConstants
    {
        public const string ApplicationEnvironmentVariableName = "ASPNETCORE_ENVIRONMENT";
        public const string ApplicationName = "DotnetNuget";
        public const string DefaultLogLocation = @"D:\home\LogFiles\Application";
        public const string ApiVersion1 = "1";

        public static List<string> ApiList() => new List<string>
        {
            ApiVersion1,
        };

        public static string CurrentApiVersion() => ApiVersion1;
    }
}