using System;

namespace DotnetNuget.Common.Library.Common.SecurityControl
{
    public class TokenResponseItem
    {
        public string TokenId { get; set; }
        public string TokenType { get; set; }
        public DateTime Issued { get; set; }
        public DateTime Expires { get; set; }

    }
}