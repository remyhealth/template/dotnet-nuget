using System.Collections.Generic;

namespace DotnetNuget.Common.Library.Common.SecurityControl
{
    public class PermissionResponse
    {
        public IList<PermissionResponseItem> Responses { get; set; }
        public int Total { get; set; }
        public int Index { get; set; }
        public int PageSize { get; set; }
        public string Next { get; set; }
        public string Previous { get; set; }

    }
}