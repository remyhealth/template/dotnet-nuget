using System;

namespace DotnetNuget.Common.Library.Common.SecurityControl
{
    public class PermissionResponseItem
    {
        public string PermissionName { get; set; }
        public string Description { get; set; }
        public DateTime Created { get; set; }
        public bool? Deleted { get; set; }
    }

}