using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace DotnetNuget.Common.Library.Common.ParserControl
{
    public class ArgumentParser : IArgumentParser
    {
        public bool CanParseBody(string arg, out IList<string> val)
        {
            if (string.IsNullOrWhiteSpace(arg))
            {
                val = new List<string>();
                return false;
            }

            return CanParseBody(new[] { arg }, out val);
        }

        public bool CanParseBody(IList<string> args, out IList<string> val)
        {
            try
            {
                if (args != null && args.Any())
                {
                    val = args;
                    return true;
                }

                val = new List<string>();
                return false;
            }
            catch (Exception)
            {
                val = new List<string>();
                return false;
            }
        }

        public bool CanParseBody(Stream request, out IList<string> val)
        {
            try
            {
                if (request == null)
                {
                    val = new List<string>();
                    return false;
                }

                var requestBody = new StreamReader(request).ReadToEnd();
                if (!string.IsNullOrWhiteSpace(requestBody))
                {
                    var escaped = System.Text.RegularExpressions.Regex.Unescape(requestBody);
                    val = new List<string>
                    {
                        escaped
                    };
                    return true;
                }

                val = new List<string>();
                return false;
            }
            catch (Exception)
            {
                val = new List<string>();
                return false;
            }
        }

        public bool Contains(string arg, IList<string> val)
        {
            if (string.IsNullOrWhiteSpace(arg))
            {
                return false;
            }

            return val.Any(i => i.Equals(arg, StringComparison.OrdinalIgnoreCase));
        }
    }
}