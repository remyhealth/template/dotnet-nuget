namespace DotnetNuget.Common.Library.Common.EnvironmentControl
{
    public interface IApplicationIdUtility
    {
        string GetApplicationId(string applicationId);
    }
}