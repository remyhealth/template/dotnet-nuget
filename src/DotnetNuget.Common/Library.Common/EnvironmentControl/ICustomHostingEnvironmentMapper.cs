namespace DotnetNuget.Common.Library.Common.EnvironmentControl
{
    public interface ICustomHostingEnvironmentMapper
    {
        ICustomHostingEnvironment Get();
    }
}