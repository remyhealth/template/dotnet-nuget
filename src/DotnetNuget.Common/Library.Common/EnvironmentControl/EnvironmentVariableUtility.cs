using System;
using System.Collections.Generic;

namespace DotnetNuget.Common.Library.Common.EnvironmentControl
{
    public class EnvironmentVariableUtility : IEnvironmentVariableUtility
    {
        public string Get(string key)
        {
            var machineVariable = Environment.GetEnvironmentVariable(key, EnvironmentVariableTarget.Machine);

            if (!string.IsNullOrWhiteSpace(machineVariable))
            {
                return machineVariable;
            }

            var userVariable = Environment.GetEnvironmentVariable(key, EnvironmentVariableTarget.User);

            if (!string.IsNullOrWhiteSpace(userVariable))
            {
                return userVariable;
            }

            var processVariable = Environment.GetEnvironmentVariable(key, EnvironmentVariableTarget.Process);

            if (!string.IsNullOrWhiteSpace(processVariable))
            {
                return processVariable;
            }

            throw new KeyNotFoundException($"Missing Key: {key}");
        }

        public string SafeGet(string key, string defaultValue = null)
        {
            try
            {
                return Get(key);
            }
            catch (KeyNotFoundException)
            {
                return defaultValue;
            }
        }

        public void Set(string key, string value) =>
            Environment.SetEnvironmentVariable(key, null,
                EnvironmentVariableTarget.Process);
    }
}