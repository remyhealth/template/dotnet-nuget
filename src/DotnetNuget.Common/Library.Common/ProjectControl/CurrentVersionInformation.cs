namespace DotnetNuget.Common.Library.Common.ProjectControl
{
    public class CurrentVersionInformation : ICurrentVersionInformation
    {
        public string ApiVersion { get; set; }
        public string ProjectVersion { get; set; }
        public string GitCommitHash { get; set; }
    }
}