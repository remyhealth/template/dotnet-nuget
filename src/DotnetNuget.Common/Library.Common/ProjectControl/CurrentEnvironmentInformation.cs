using DotnetNuget.Common.Library.Common.EnvironmentControl;

namespace DotnetNuget.Common.Library.Common.ProjectControl
{
    public class CurrentEnvironmentInformation : ICurrentEnvironmentInformation
    {
        public string Url { get; set; }
        public EnvironmentEnum Environment { get; set; }
        public string Name { get; set; }
        public ICurrentThirdPartyInformation Authentication { get; set; }
    }
}