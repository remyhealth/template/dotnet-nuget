namespace DotnetNuget.Common.Library.Common.ProjectControl
{
    public interface IProjectInformation
    {
        string ApplicationName { get; set; }
        ICurrentVersionInformation VersionInformation { get; set; }
    }
}