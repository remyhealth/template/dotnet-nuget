namespace DotnetNuget.Common.Library.Common.ProjectControl
{
    public interface ICurrentVersionInformation
    {
        string ApiVersion { get; set; }
        string ProjectVersion { get; set; }
        string GitCommitHash { get; set; }
    }
}