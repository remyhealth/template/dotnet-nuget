using System;
using DotnetNuget.Common.Library.Common.EnvironmentControl;

namespace DotnetNuget.Common.Library.Common.ProjectControl
{
    public class ProjectInformationMapper : IProjectInformationMapper
    {
        public ICurrentProjectInformation MapCurrent(AppSettings appSettings, string currentApiVersion, EnvironmentEnum environment = EnvironmentEnum.Unknown)
        {
            if (appSettings == null)
            {
                return DefaultCurrentProjectInformation();
            }

            return new CurrentProjectInformation
            {
                ApplicationName = appSettings.ApplicationName,
                VersionInformation = GetVersionInformation(appSettings, currentApiVersion),
                CurrentEnvironment = GetEnvironment(appSettings.EnvironmentInformation, environment)
            };
        }

        public IHealthProjectInformation MapHealth(ICurrentProjectInformation currentProjectInformation)
        {
            if (currentProjectInformation == null)
            {
                return DefaultHealthProjectInformation();
            }

            return new HealthProjectInformation
            {
                ApplicationName = currentProjectInformation.ApplicationName,
                VersionInformation = currentProjectInformation.VersionInformation,
                CurrentEnvironment = MapHealthCurrentEnvironment(currentProjectInformation.CurrentEnvironment)
            };
        }

        private ICurrentVersionInformation GetVersionInformation(AppSettings appSettings, string currentApiVersion) =>
            new CurrentVersionInformation
            {
                ApiVersion = currentApiVersion,
                GitCommitHash = appSettings.VersionInformation?.GitCommitHash,
                ProjectVersion = appSettings.VersionInformation?.ProjectVersion
            };


        private IHealthProjectInformation DefaultHealthProjectInformation() =>
            new HealthProjectInformation
            {
                ApplicationName = "Unknown",
                VersionInformation = new CurrentVersionInformation
                {
                    ApiVersion = "0",
                    ProjectVersion = "0.0.0",
                    GitCommitHash = "0000000000000000000000000000000000000000"
                },
                CurrentEnvironment = DefaultHealthEnvironmentInformation()
            };

        private ICurrentProjectInformation DefaultCurrentProjectInformation() =>
            new CurrentProjectInformation()
            {
                ApplicationName = "Unknown",
                VersionInformation = new CurrentVersionInformation
                {
                    ApiVersion = "0",
                    ProjectVersion = "0.0.0",
                    GitCommitHash = "0000000000000000000000000000000000000000"
                },
                CurrentEnvironment = new CurrentEnvironmentInformation
                {
                    Environment = EnvironmentEnum.Unknown,
                    Name = Enum.GetName(typeof(EnvironmentEnum), EnvironmentEnum.Unknown),
                    Authentication = new CurrentThirdPartyInformation()
                }
            };

        private IHealthEnvironmentInformation MapHealthCurrentEnvironment(ICurrentEnvironmentInformation currentEnvironment)
        {
            if (currentEnvironment == null)
            {
                return DefaultHealthEnvironmentInformation();
            }

            return new HealthEnvironmentInformation
            {
                Url = currentEnvironment.Url,
                Environment = currentEnvironment.Environment,
                Name = Enum.GetName(typeof(EnvironmentEnum), currentEnvironment.Environment)
            };
        }

        private IHealthEnvironmentInformation DefaultHealthEnvironmentInformation() =>
            new HealthEnvironmentInformation
            {
                Url = null,
                Environment = EnvironmentEnum.Unknown,
                Name = Enum.GetName(typeof(EnvironmentEnum), EnvironmentEnum.Unknown)
            };

        private ICurrentEnvironmentInformation GetEnvironment(AppSettingsEnvironment environmentInformation, EnvironmentEnum environment)
        {
            switch (environment)
            {
                case EnvironmentEnum.Production:
                    return new CurrentEnvironmentInformation
                    {
                        Url = environmentInformation.Production.Url,
                        Environment = EnvironmentEnum.Production,
                        Name = Enum.GetName(typeof(EnvironmentEnum), EnvironmentEnum.Production),
                        Authentication = GetThirdPartyInformation(environmentInformation.Production)
                    };
                case EnvironmentEnum.Staging:
                    return new CurrentEnvironmentInformation
                    {
                        Url = environmentInformation.Staging.Url,
                        Environment = EnvironmentEnum.Staging,
                        Name = Enum.GetName(typeof(EnvironmentEnum), EnvironmentEnum.Staging),
                        Authentication = GetThirdPartyInformation(environmentInformation.Staging)
                    };
                case EnvironmentEnum.Uat:
                    return new CurrentEnvironmentInformation
                    {
                        Url = environmentInformation.Uat.Url,
                        Environment = EnvironmentEnum.Uat,
                        Name = Enum.GetName(typeof(EnvironmentEnum), EnvironmentEnum.Uat),
                        Authentication = GetThirdPartyInformation(environmentInformation.Uat)
                    };
                case EnvironmentEnum.Development:
                    return new CurrentEnvironmentInformation
                    {
                        Url = environmentInformation.Development.Url,
                        Environment = EnvironmentEnum.Development,
                        Name = Enum.GetName(typeof(EnvironmentEnum), EnvironmentEnum.Development),
                        Authentication = GetThirdPartyInformation(environmentInformation.Development)
                    };
                case EnvironmentEnum.Local:
                    return new CurrentEnvironmentInformation
                    {
                        Url = environmentInformation.Local.Url,
                        Environment = EnvironmentEnum.Local,
                        Name = Enum.GetName(typeof(EnvironmentEnum), EnvironmentEnum.Local),
                        Authentication = GetThirdPartyInformation(environmentInformation.Local)
                    };
                case EnvironmentEnum.Unknown:
                    throw new Exception("Environment Unknown");
                default:
                    throw new Exception("Environment Unknown");
            }
        }

        private EnvironmentEnum GetEnvironmentEnum(string environmentName)
        {
            if (string.IsNullOrWhiteSpace(environmentName))
            {
                return EnvironmentEnum.Unknown;
            }

            var env = environmentName.ToLowerInvariant().Trim();

            switch (env)
            {
                case "production":
                    return EnvironmentEnum.Production;
                case "staging":
                    return EnvironmentEnum.Staging;
                case "uat":
                    return EnvironmentEnum.Uat;
                case "development":
                    return EnvironmentEnum.Development;
                case "local":
                    return EnvironmentEnum.Local;
                default:
                    return EnvironmentEnum.Unknown;
            }
        }

        private ICurrentThirdPartyInformation GetThirdPartyInformation(AppSettingsCurrentEnvironment environmentInformation) =>
            new CurrentThirdPartyInformation
            {
                Audience = environmentInformation.Authentication?.Audience,
                ClientId = environmentInformation.Authentication?.ClientId,
                Domain = environmentInformation.Authentication?.Domain,
                Issuer = environmentInformation.Authentication?.Issuer,
                Key = environmentInformation.Authentication?.Key,
                TenantId = environmentInformation.Authentication?.TenantId
            };
    }
}