
namespace DotnetNuget.Common.Library.Common.ProjectControl
{
    public interface ICurrentEnvironmentInformation : IHealthEnvironmentInformation
    {
        ICurrentThirdPartyInformation Authentication { get; set; }
    }
}