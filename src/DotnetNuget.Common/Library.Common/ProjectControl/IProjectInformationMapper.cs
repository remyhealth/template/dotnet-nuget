using DotnetNuget.Common.Library.Common.EnvironmentControl;

namespace DotnetNuget.Common.Library.Common.ProjectControl
{
    public interface IProjectInformationMapper
    {
        ICurrentProjectInformation MapCurrent(AppSettings appSettings, string currentApiVersion,
            EnvironmentEnum environment = EnvironmentEnum.Unknown);

        IHealthProjectInformation MapHealth(ICurrentProjectInformation currentProjectInformation);
    }
}