namespace DotnetNuget.Common.Library.Common.DatabaseControl
{
    public interface IConnection
    {
        string ConnectionString { get; }
    }
}