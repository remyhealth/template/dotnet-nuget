using System.Collections.Generic;
using DotnetNuget.Bootstrapper.Library.Bootstrapper;
using DotnetNuget.Common.Library.Common.ParserControl;
using DotnetNuget.SDK;
using Microsoft.Extensions.DependencyInjection;

namespace DotnetNuget.Bootstrapper
{
    public class Registrations
    {
        //https://docs.microsoft.com/en-us/aspnet/core/fundamentals/dependency-injection?view=aspnetcore-2.2#service-lifetimes
        public static IList<ServiceDescriptor> Load() => Load(new ServiceCollection());

        public static IList<ServiceDescriptor> Load(IServiceCollection services)
        {
            services.AddSingleton<IArgumentParser, ArgumentParser>();
            services.AddSingleton<IConsoleRunner, ConsoleRunner>();
            services.AddSingleton<IRunner, DotnetNugetRunner>();
            services.AddTransient<IDotnetNugetService, DotnetNugetService>();

            return services;
        }
    }
}