using System.Collections.Generic;
using System.Threading.Tasks;

namespace DotnetNuget.Bootstrapper.Library.Bootstrapper
{
    public interface IConsoleRunner
    {
        Task Run(IList<string> args, IRunner runner);
    }
}