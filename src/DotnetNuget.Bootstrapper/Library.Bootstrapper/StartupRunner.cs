using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Autofac;
using DotnetNuget.Bootstrapper.Library.Bootstrapper;
using Microsoft.Extensions.DependencyInjection;

namespace DotnetNuget.Bootstrapper.Library.Bootstrapper
{
    public class StartupRunner
    {
        public void Run(IList<string> args, IServiceCollection services) =>
            Run(args, new BootstrapperRegistration()
                .RegisterAutofac(services)
                .Build());

        public void Run(IList<string> args, IContainer container)
        {
            using (var scope = container.BeginLifetimeScope())
            {
                RunLoop(args, scope);
            }
        }

        private void RunLoop(IList<string> args, ILifetimeScope scope)
        {
            var runners = scope.Resolve<IEnumerable<IRunner>>();

            if (runners == null)
            {
                throw new Exception("No runners registered");
            }

            var list = runners.ToList();

            if (!list.Any())
            {
                throw new Exception("No runners implemented");
            }

            var tasks = list
                .Select(runner => runner.Run(args))
                .ToList();

            Task.WaitAll(tasks.ToArray());
        }
    }
}